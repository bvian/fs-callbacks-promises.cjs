const { readFile, upperCaseFile, lowerCaseFile, sortFilesContent, deleteFiles } = require('../problem2.cjs');

const upperCaseFileName = "upperCaseFile.txt";
const lowerCaseFileName = "lowerCaseFile.txt";
const sortedFileName = "sortedFile.txt";

readFile() // function to read file lipsum.txt file
    .then((fileContent) => {
        return upperCaseFile(upperCaseFileName, fileContent); // function to convert all the contents into uppercase and store it into new txt file
    })
    .then((upperCaseFileContent) => {
        return lowerCaseFile(lowerCaseFileName, upperCaseFileContent); // function to convert all the contents into lowercase, splitting it into new line and store it into new txt file
    })
    .then(() => {
        return sortFilesContent(sortedFileName, upperCaseFileName, lowerCaseFileName);
    })
    .then(() => {
        return deleteFiles(); // delete all the files according to the filename.txt
    })
    .then(() => {
        console.log("All new files deleted");
    })
    .catch((err) => {
        console.error(err);
    });